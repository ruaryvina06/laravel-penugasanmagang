@extends('admin.index')
@section('isi-contentAdmin')
    <!-- Basic Bootstrap Table -->
    <div class="card">
        <h5 class="card-header">Tabel Report Tracker</h5>
        <div class="table-responsive text-nowrap" style="margin-right: 3%; margin-left: 3%">
            <table class="table data-table" id="report-trackers-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID User</th>
                        <th>ID Report</th>
                        <th>Status</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    {{-- @foreach ($tracker as $t)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $t->userfk->name }}</td>
                            <td>{{ $t->reportfk->title }}</td>
                            <td>{{ $t->status }}</td>
                            <td>{{ $t->note }}</td>
                        </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
    </div>
    <!--/ Basic Bootstrap Table -->
@endsection

@push('js-custom')
    <script>
        $(document).ready(function() {
            $('#report-trackers-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('reportTrackerJson') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'userfk.name',
                        name: 'userfk.name'
                    },
                    {
                        data: 'reportfk.title',
                        name: 'reportfk.title'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'note',
                        name: 'note'
                    },
                ],
            });
        });
    </script>
    {{-- <script>
        $(document).ready(function() {
            $('#reportTracker-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin-reportTracker') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'userfk.name',
                        name: 'userfk.name'
                    },
                    {
                        data: 'reportfk.title',
                        name: 'reportfk.title'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'note',
                        name: 'note'
                    },

                ]
            });
        });
    </script> --}}
@endpush
