@extends('admin.index')
@section('isi-contentAdmin')
    <!-- Basic Bootstrap Table -->
    <div class="card">
        <h5 class="card-header">Tabel Report </h5>
        <div class="table-responsive text-nowrap"style="margin-right: 3%; margin-left: 3%">
            <table class="table " id="report-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Pelapor</th>
                        <th>Kategori</th>
                        <th>ID Tiket </th>
                        <th>Descripsi</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    {{-- @foreach ($record as $r)
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $r->reporterfk->name }}</td>
                        <td>{{ $r->kategorifk->name }}</td>
                        <td>{{ $r->ticket_id }}</td>
                        <td>{{ $r->description }}</td>
                        <td>{{ $r->status }}</td>
                        <td>
                            <a href="{{ route('admin-report-edit', $r->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <a href="" class="btn btn-danger btn-sm">Hapus</a>
                        </td>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
    </div>
    <!--/ Basic Bootstrap Table -->
@endsection

@push('js-custom')
    <script>
        $(document).ready(function() {
            $('#report-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('reportJson') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'reporterfk.name',
                        name: 'reporterfk.name'
                    },
                    {
                        data: 'kategorifk.name',
                        name: 'kategorifk.name'
                    },
                    {
                        data: 'ticket_id',
                        name: 'ticket_id'
                    },
                    {
                        data: 'description',
                        name: 'description'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    </script>
@endpush
