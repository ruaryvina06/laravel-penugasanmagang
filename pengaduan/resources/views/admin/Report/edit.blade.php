@extends('admin.index')
@section('isi-contentAdmin')
    <!-- Basic Bootstrap Table -->
    <div class="card">
        <h5 class="card-header">Table Basic</h5>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="form-contact contact_form" action="{{ route('admin-report-update', $record->id) }}"
                        method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12">
                            <label for="name" class="form-label">Reporter</label>
                            <input type="text" class="form-control" value="{{ $record->reporter_id }}" id="name"
                                name="name" placeholder="Enter your name">
                        </div>
                        <div class="col-12">
                            <label for="email" class="form-label">Category</label>
                            {{-- <input type="email" class="form-control" value="{{ $record->category_id }}" id="email"
                                name="email" placeholder="Enter your email"> --}}
                            <select id="identity_type" class="form-select" name="kategori">
                                @foreach ($kategori as $k)
                                    @if ($k->id == $record->category_id)
                                        <option value="{{ $k->id }}" selected>{{ $k->name }}</option>
                                    @else
                                        <option value="{{ $k->id }}">{{ $k->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <label for="phone_number" class="form-label">Ticket ID</label>
                            <input type="text" class="form-control" value="{{ $record->ticket_id }}" id="phone_number"
                                name="phone_number" placeholder="08***">
                        </div>
                        <div class="col-12">
                            <label for="phone_number" class="form-label">Judul</label>
                            <input type="text" class="form-control" value="{{ $record->title }}" id="phone_number"
                                name="phone_number" placeholder="08***">
                        </div>
                        <div class="col-12">
                            <label for="description" class="form-label">Description</label>
                            <textarea class="form-control" id="description" name="description" rows="3">{{ $record->description }}</textarea>
                        </div>
                        <div class="col-12 ">
                            <label for="identity_type" class="form-label">Status</label>
                            <select id="identity_type" class="form-select" name="status">
                                @foreach ($status as $s)
                                    @if ($s == $record->status)
                                        <option value="{{ $s }}" selected>{{ $s }}</option>
                                    @else
                                        <option value="{{ $s }}">{{ $s }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12 mb-3">
                            <label for="description" class="form-label">NOTE*</label>
                            <textarea class="form-control" id="description" name="note" rows="3"></textarea>
                        </div>
                        <div class="col-12 mb-5">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                            <a type="submit" href="{{ route('admin-report') }}" class="btn btn-info">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/ Basic Bootstrap Table -->
@endsection
