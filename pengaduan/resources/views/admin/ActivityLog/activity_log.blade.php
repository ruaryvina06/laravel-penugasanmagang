@extends('admin.index')
@section('isi-contentAdmin')
    <!-- Basic Bootstrap Table -->
    <div class="card">
        <h5 class="card-header">Table Basic</h5>
        <div class="table-responsive text-nowrap" style="margin-right: 3%; margin-left: 3%">
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>User</th>
                        <th>Event</th>
                        <th>Before</th>
                        <th>After</th>
                        <th>Description</th>
                        <th>Log At</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($log as $l)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $l->causer->name }}</td>
                            <td>{{ $l->event }}</td>
                            <td>
                                @php
                                    $hasil = json_decode($l->properties, true)['old'];
                                    echo json_encode($hasil);
                                @endphp
                            </td>
                            <td>
                                @php
                                    $hasil = json_decode($l->properties, true)['attributes'];
                                    echo json_encode($hasil);
                                @endphp
                            </td>
                            <td>{{ $l->description }}</td>
                            <td>{{ $l->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!--/ Basic Bootstrap Table -->
@endsection
