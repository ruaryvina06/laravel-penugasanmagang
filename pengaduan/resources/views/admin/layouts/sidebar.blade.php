<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="index.html" class="app-brand-link">
            <span class="app-brand-text demo menu-text fw-bolder ms-2">Consulto</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item {{ request()->route()->getName() === 'dashboard-admin'? 'active': '' }}">
            <a href="{{ route('dashboard-admin') }}" class="menu-link ">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>
        <li class="menu-item  {{ request()->route()->getName() === 'admin-reportTracker'? 'active': '' }}">
            <a href="{{ route('admin-reportTracker') }}" class="menu-link ">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div data-i18n="Basic">Report Tracker</div>
            </a>
        </li>
        <li class="menu-item  {{ request()->route()->getName() === 'admin-report'? 'active': '' }}">
            <a href="{{ route('admin-report') }}" class="menu-link ">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div data-i18n="Basic">Report</div>
            </a>
        </li>
        <li class="menu-item {{ request()->route()->getName() === 'admin-activity-log'? 'active': '' }}">
            <a href="{{ route('admin-activity-log') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-collection"></i>
                <div data-i18n="Basic">Activity Log</div>
            </a>
        </li>
    </ul>
</aside>
