<header>
    <!-- Header Start -->
    <div class="header-area header_area">
        <div class="main-header">
            <div class="header-bottom  header-sticky">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2 col-md-1">
                            <div class="logo">
                                <a href="index.html"><img src="{{ asset('landing/assets/img/logo/logo.png') }}"
                                        alt=""></a>
                            </div>

                        </div>
                        <div class="col-xl-10 col-lg-10 col-md-8">
                            <div class="header-left  d-flex f-right align-items-center">
                                <!-- Main-menu -->
                                <div class="main-menu f-right d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li class="{{ request()->route()->getName() === 'beranda'? 'active': '' }}">
                                                <a href="{{ route('beranda') }}">Beranda</a>
                                            </li>
                                            <li class="{{ request()->route()->getName() === 'beranda'? 'active': '' }}">
                                                <a href="{{ route('beranda') }}">Tentang Kami</a>
                                            </li>
                                            <li class="{{ request()->route()->getName() === 'lapor'? 'active': '' }}">
                                                <a href="{{ route('lapor') }}">Statistik</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>
