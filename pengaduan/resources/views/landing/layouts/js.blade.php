<script src="{{ asset('landing/assets/js/vendor/modernizr-3.5.0.min.js') }}"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="{{ asset('landing/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/bootstrap.min.js') }}"></script>
<!-- Jquery Mobile Menu -->
<script src="{{ asset('landing/assets/js/jquery.slicknav.min.js') }}"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="{{ asset('landing/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/slick.min.js') }}"></script>
<!-- One Page, Animated-HeadLin -->
<script src="{{ asset('landing/assets/js/wow.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/animated.headline.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.magnific-popup.js') }}"></script>

<!-- Date Picker -->
<script src="{{ asset('landing/assets/js/gijgo.min.js') }}"></script>
<!-- Nice-select, sticky -->
<script src="{{ asset('landing/assets/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.sticky.js') }}"></script>
<!-- Progress -->
<script src="{{ asset('landing/assets/js/jquery.barfiller.js') }}"></script>

<!-- counter , waypoint,Hover Direction -->
<script src="{{ asset('landing/assets/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/waypoints.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/hover-direction-snake.min.js') }}"></script>

<!-- contact js -->
<script src="{{ asset('landing/assets/js/contact.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.form.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('landing/assets/js/mail-script.js') }}"></script>
<script src="{{ asset('landing/assets/js/jquery.ajaxchimp.min.js') }}"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="{{ asset('landing/assets/js/plugins.js') }}"></script>
<script src="{{ asset('landing/assets/js/main.js') }}"></script>
