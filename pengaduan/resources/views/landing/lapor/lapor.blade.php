@extends('landing.index')
@section('isi-contentLanding')
    <section class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title">Formulir Pelaporan</h2>
                </div>
                <div class="col-lg-8 ">
                    <form class="form-contact contact_form" action="{{ route('lapor-store') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="col-12 mb-2">
                            <label for="name" class="form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="name" name="name"
                                placeholder="Masukkan nama anda">
                        </div>
                        <div class="col-12 mb-2">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                placeholder="Masukkan email anda">
                        </div>
                        <div class="col-12 mb-2">
                            <label for="phone_number" class="form-label">Nomor Telepon</label>
                            <input type="text" class="form-control" id="phone_number" name="phone_number"
                                placeholder="08***">
                        </div>
                        <div class="row">
                            <div class="col-6 mb-2">
                                <label for="identity_type" class="form-label">Tipe Identitas</label>
                                <select id="identity_type" class="form-select" name="identity_type">
                                    <option selected>Pilih...</option>
                                    <option value="KTP">KTP</option>
                                    <option value="SIM">SIM</option>
                                </select>
                            </div>
                            <div class="col-6 mb-2">
                                <label for="identity_number" class="form-label">Nomor Identitas</label>
                                <input type="text" class="form-control" id="identity_number" name="identity_number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 mb-2">
                                <label for="pob" class="form-label">Tempat Lahir</label>
                                <input type="text" class="form-control" id="pob" name="pob"
                                    placeholder="Kota/Provinsi*">
                            </div>
                            <div class="col-6 mb-2">
                                <label for="dob" class="form-label">Tanggal lahir</label>
                                <input type="date" class="form-control" id="dob" name="dob">
                            </div>
                        </div>
                        <div class="col-12 mb-2">
                            <label for="address" class="form-label">Alamat</label>
                            <input type="text" class="form-control" id="address" name="address"
                                placeholder="Masukkan alamat anda">
                        </div>
                        <div class="col-12 mb-2">
                            <label for="title" class="form-label">Judul Laporan </label>
                            <input type="text" class="form-control" id="title" name="title"
                                placeholder="Masukkan judul laporan">
                        </div>
                        <div class="col-12 mb-3">
                            <label for="description" class="form-label">Deskripsi</label>
                            <textarea class="form-control" id="description" name="description" rows="3" placeholder="Tulis deskripsi laporan"></textarea>
                        </div>
                        <div class="col-12 mb-3">
                            <label for="media" class="form-label">Media</label>
                            <input class="form-control" type="file" id="media" name="media" multiple
                                placeholder="Masukkan bukti pendukung">
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Ajukan</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1">
                </div>
            </div>
        </div>
    </section>
@endsection
<!--?  JS Custom -->
@push('js-custom')
    <script>
        $('.file-upload').file_upload();
    </script>
@endpush
