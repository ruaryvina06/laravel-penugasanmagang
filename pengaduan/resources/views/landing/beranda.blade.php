@extends('landing.index')
@section('isi-contentLanding')
    <!--? slider Area Start-->
    <div class="slider-area  slider-height position-relative"
        data-background="{{ asset('landing/assets/img/hero/h1_hero.png') }}">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider">
                <div class="slider-cap-wrapper">
                    <div class="hero-caption">
                        <span>Pelayanan Terbaik</span>
                        <h1 data-animation="fadeInLeft" data-delay=".4s">Pelaporan Masyarakat</h1>
                        <p data-animation="fadeInLeft" data-delay=".6s">Nikmati kemudahan anda dalam menyampaikan aspirasi dan
                            pengaduan
                        </p>
                        <!-- Hero Btn -->
                        <a href="#" class="btn" data-animation="fadeInLeft" data-delay=".9s">Jelajah</a>
                    </div>
                    <div class="hero-img">
                        <img src="{{ asset('landing/assets/img/hero/h1_hero1.png') }}" alt=""
                            data-animation="fadeInRight" data-transition-duration="5s">
                    </div>
                </div>
            </div>
        </div>
        <!-- Video icon -->
        <div class="video-icon">
            <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0"><i
                    class="fas fa-play"></i></a>
        </div>
    </div>
    <!-- slider Area End-->
    <!--? Services Area Start -->
    <section class="services-section section-padding30 fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12">
                    <!-- Section Tittle -->
                    <div class="section-tittle text-center mb-70">
                        <span>Pelayanan Kami</span>
                        <h2>Kami memberikan kemudahan untuk melaporkan masalah dengan cepat dan efisien</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Services Area End -->
    <!--! About Area Start 01 -->
    <section class="about-area fix">
        <!--Right Contents  -->
        <div class="about-img">

        </div>
        <!-- left Contents -->
        <div class="about-details">
            <div class="right-caption">
                <!-- Section Tittle -->
                <div class="section-tittle mb-50">
                    <h2>Tentang<br>Kami</h2>
                </div>
                <div class="about-more">
                    <p>Consulto merupakan sebuah platform digital yang dirancang khusus untuk memberikan kemudahan
                        kepada masyarakat dalam melaporkan berbagai masalah yang mereka temui dalam kehidupan sehari-hari.
                        Tujuan utama aplikasi ini adalah untuk mempermudah, mempercepat, dan meningkatkan transparansi dalam
                        proses pelaporan masalah. </p>
                    <p class="pera-bottom">Sehingga masyarakat dapat berperan aktif dalam menjaga dan meningkatkan
                        kualitas lingkungan mereka, sambil meningkatkan efisiensi pemerintah dalam menangani permasalahan
                        yang dilaporkan.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- About Area End-->
    <!--! About Area Start 02-->
    <div class="about-area2">
        <!-- left Contents -->
        <div class="about-details2">
            <div class="right-caption2">
                <!-- Section Tittle -->
                <div class="section-tittle mb-50">
                    <h2>Keunggulan<br>Kami</h2>
                </div>
                <!-- collapse-wrapper -->
                <div class="collapse-wrapper">
                    <div class="accordion" id="accordionExample">
                        <!-- single-one -->
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <a href="#" class="btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false"
                                        aria-controls="collapseTwo">Tampilan yang menarik.</a>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Antarmuka aplikasi dirancang dengan tampilan yang menarik dan ramah pengguna.
                                    Desain yang estetis membuat pengalaman pengguna lebih menyenangkan dan intuitif,
                                    sehingga pengguna merasa nyaman saat menggunakan aplikasi kami.
                                </div>
                            </div>
                        </div>
                        <!-- single-two -->
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <a href="#" class="btn-link" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">Kemudahan dalam melapor.</a>
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Salah satu keunggulan utama kami adalah kemudahan dalam pelaporan masalah. Pengguna
                                    dapat melaporkan masalah dengan mudah dan cepat.
                                </div>
                            </div>
                        </div>
                        <!-- single-three -->
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <a href="#" class="btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">Status pelaporan yang jelas.</a>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                    Kami memberikan keunggulan dengan fitur status tracking laporan yang memungkinkan
                                    pengguna untuk mengikuti perkembangan laporan mereka.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Right Contents  -->
        <div class="about-img2">
        </div>
    </div>
    <!-- About Area End-->

    <!--? Team Ara Start -->
    <!--? Want To work -->
    <section class="wantToWork-area section-bg2"
        data-background="{{ asset('landing/assets/img/gallery/section_bg01.png') }}">
        <div class="container">
            <div class="wants-wrapper w-padding2">
                <div class="row align-items-center justify-content-between">
                    <div class="col-xl-7 col-lg-7 col-md-12">
                        <div class="wantToWork-caption wantToWork-caption2">
                            <h2>Siap Lapor !!</h2>
                            <p>Kami memberikan kemudahan untuk merasa bangga menjadi bagian dari gerakan menuju perubahan
                                yang lebih baik.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Want To work End -->
@endsection
