<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportTrackers extends Model
{
    use HasFactory;
    protected $table = 'report_trackers';
    protected $guarded = [];
    public function reportfk()
    {
        return $this->belongsTo(Report::class, 'report_id', 'id');
    }
    public function userfk()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
