<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    use HasFactory;

    protected $table = 'activity_log';
    protected $guarded = [];

    public function subject()
    {
        return $this->belongsTo(Report::class, 'subject_id', 'id');
    }
    public function causer()
    {
        return $this->belongsTo(User::class, 'causer_id', 'id');
    }
}
