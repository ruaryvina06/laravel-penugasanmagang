<?php

namespace App\Http\Controllers;

use App\Models\ReportTrackers;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ReporTrackerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tracker = ReportTrackers::all();

        return view('admin.ReportTracker.report_tracker', compact('tracker'));
    }
    public function reportTraJson(Request $request)
    {
        $reportTrackers = ReportTrackers::with(['userfk', 'reportfk'])->select('report_trackers.*');

        return DataTables::eloquent($reportTrackers)
            ->addColumn('DT_RowIndex', function () use (&$index) {
                return ++$index; // Menambahkan dan mengembalikan nomor urut
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $record = ReportTrackers::orderBy('id', 'desc')->get();
        return view('admin.ReportTracker.report_tracker', compact('record'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
