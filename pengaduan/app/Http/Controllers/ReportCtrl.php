<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Report;
use App\Models\Reporters;
use App\Models\ReportTrackers;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ReportCtrl extends Controller
{
    public function index()
    {
        return view('landing.lapor.lapor');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'identity_type' => 'required',
            'identity_number' => 'required',
            'phone_number' => 'required',
            'pob' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'description' => 'required',
            'media' => 'required',
        ]);
        // dd($request->all());
        $reporterID =  Reporters::insertGetId([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'identity_type' => $request->identity_type,
            'identity_number' => $request->identity_number,
            'pob' => $request->pob,
            'dob' => $request->dob,
            'address' => $request->address,
        ]);

        $lastReport = Report::latest('id')->first();
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date("Ymd");

        if ($lastReport) {
            $urutan = substr($lastReport->ticket_id, 8);
            $new = (int)$urutan + 1;
            $idTiket = $tanggal . sprintf("%04s", $new);
        } else {
            $idTiket = $tanggal . '0001';
        }


        $report = Report::create([
            'reporter_id' => $reporterID,
            'category_id' => $request->category_id,
            'ticket_id' => $idTiket,
            'title' => $request->title,
            'description' => $request->description,
            'status' => 'Pending'
        ]);

        $report->addMediaFromRequest('media')->toMediaCollection();

        return redirect('lapor');
    }

    public function show()
    {
        $record = Report::orderBy('id', 'desc')->get();
        return view('admin.Report.report', compact('record'));
    }

    public function edit($id)
    {
        $record = Report::where('id', $id)->get()[0];
        $kategori = Categories::all();
        $status = ['Pending', 'Proses Administratif', 'Proses Penanganan', 'Selesai Ditangani', 'Laporan Ditolak'];
        // dd($record);
        return view('admin.Report.edit', compact('record', 'kategori', 'status'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
        ]);

        $report = Report::findOrFail($id);
        $report->update([
            'status' => $request->status,
            'category_id' => $request->kategori,
        ]);

        ReportTrackers::create([
            'user_id' => auth()->user()->id,
            'report_id' => $id,
            'status' => $request->status,
            'note' => ($request->note) ? $request->note : '-',
        ]);

        return redirect()->route('admin-report');
    }

    public function dataJson(Request $request)
    {
        // $record = Report::limit(10)->orderBy('id', 'desc');
        // return DataTables::of($record)->make(true);
        $reports = Report::with(['reporterfk', 'kategorifk'])
            ->select('reports.*')
            ->get();
        return DataTables::of($reports)
            ->addColumn('DT_RowIndex', function () use (&$index) {
                return ++$index; // Menambahkan dan mengembalikan nomor urut
            })
            ->addColumn('action', function ($report) {
                return '<a href="' . route('admin-report-edit', $report->id) . '" class = "btn btn-info">Edit</a> <a href="' . route('admin-report-hapus', $report->id) . '" class = "btn btn-danger">Hapus</a>';
            })

            ->rawColumns(['DT_RowIndex', 'action'])
            ->make(true);
    }
    public function destroy($id)
    {
        $data = Report::findOrFail($id);
        $data->delete();

        return redirect()->route('admin-report');
    }
}
