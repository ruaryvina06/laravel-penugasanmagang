<?php

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            'Infrastruktur',
            'Lingkungan',
            'Layanan Publik',
            'Keamanan',
            'Kesehatan',
            'Lainnya',
        ];
        foreach ($categories as $category) {
            Categories::create([
                'name' => $category,
                'slug' => $category
            ]);
        }
    }
}
