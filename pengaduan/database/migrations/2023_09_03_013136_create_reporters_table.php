<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('reporters', function (Blueprint $table) {
            $table->id();
            $table->char('name', 255);
            $table->char('email', 255);
            $table->char('phone_number', 255);
            $table->enum('identity_type', ['KTP', 'SIM']);
            $table->char('identity_number', 255);
            $table->char('pob', 255);
            $table->date('dob');
            $table->char('address', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('reporters');
    }
};
