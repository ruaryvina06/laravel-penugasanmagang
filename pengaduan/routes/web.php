<?php

use App\Http\Controllers\BerandaController;
use App\Http\Controllers\DashboardCtrl;
use App\Http\Controllers\LogActivityController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ReportCtrl;
use App\Http\Controllers\ReporTrackerController;
use App\Models\ReportTrackers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', [BerandaController::class, 'index'])->name('beranda');

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login/validation', [LoginController::class, 'login'])->name('loginValidate');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/lapor', [ReportCtrl::class, 'index'])->name('lapor');
Route::post('/lapor/store', [ReportCtrl::class, 'store'])->name('lapor-store');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [DashboardCtrl::class, 'index'])->name('dashboard-admin');
    Route::get('/report-admin', [ReportCtrl::class, 'show'])->name('admin-report');
    Route::get('/report-admin/{id}/edit', [ReportCtrl::class, 'edit'])->name('admin-report-edit');
    Route::post('/report-admin/{id}/update', [ReportCtrl::class, 'update'])->name('admin-report-update');
    Route::get('/report-admin/{id}/hapus', [ReportCtrl::class, 'destroy'])->name('admin-report-hapus');

    Route::get('/report-tracker-admin', [ReporTrackerController::class, 'show'])->name('admin-reportTracker');
    Route::get('/report-tracker/json', [ReporTrackerController::class, 'reportTraJson'])->name('reportTrackerJson');
    Route::get('/activity/log', [LogActivityController::class, 'index'])->name('admin-activity-log');

    Route::get('/report/json', [ReportCtrl::class, 'dataJson'])->name('reportJson');
});
